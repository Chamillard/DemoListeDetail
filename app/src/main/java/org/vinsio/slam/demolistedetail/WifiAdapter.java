package org.vinsio.slam.demolistedetail;

import org.vinsio.slam.demolistedetail.Wifi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Classe Helper pour fournir le contenu de la liste de composants
public class WifiAdapter {

    public static final List<Wifi> ITEMS = new ArrayList<Wifi>();
    public static final Map<String, Wifi> ITEM_MAP = new HashMap<String, Wifi>();
    private static final int COUNT = 25;

    //static {
        // Add some sample items.
     //   for (int i = 1; i <= COUNT; i++) {
            //addItem(createComposantItem(i));
     //   }
    //}
/*
    private static void addItem(Wifi item) {
        ITEMS.add(item);
        //ITEM_MAP.put(item.id, item);
    }

    private static Wifi createWifi(int position) {
        return new Wifi(String.valueOf(position), "Item " + position, makeDetails(position));
    }
*/

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details à propos de hotspot : ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nplus d'informations ici.");
        }
        return builder.toString();
    }
}
