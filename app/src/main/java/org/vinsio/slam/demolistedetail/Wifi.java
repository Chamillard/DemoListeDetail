package org.vinsio.slam.demolistedetail;

import java.io.Serializable;

public class Wifi implements Serializable {
    private String objectid;
    private String arrondissement;
    private String adresse;
    private String nom_site;
    private String code_site;

    public Wifi() {}

    public Wifi(String objectid, String arrondiassement, String adresse, String nom_site, String code_site) {
        this.objectid = objectid;
        this.arrondissement = arrondissement;
        this.adresse = adresse;
        this.nom_site = nom_site;
        this.code_site = code_site;
    }

    public String getobjectId() {
        return objectid;
    }
 
    public void setObjectId(String objectid) {
        this.objectid = objectid;
    }

    public String getArrondissement() {
        return arrondissement;
    }

    public void setArrondissement(String arrondissement) { this.arrondissement = arrondissement; }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNomSite() {
        return nom_site;
    }
 
    public void setNomSite(String nom_site) {
        this.nom_site = nom_site;
    }

    public String getCodeSite() { return code_site; }

    public void setCodeSite(String code_site) {
        this.code_site = code_site;
    }

    @Override
    public String toString() {
        return "HotSpot [ " +
                "objectid='" + objectid + '\'' +
                ", arrondissement'" + arrondissement + '\'' +
                ", adresse='" + adresse + '\'' +
                ", nom='" + nom_site + '\'' +
                ", code=" + code_site +
                ']';
    }
}